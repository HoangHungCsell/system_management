require('dotenv').config();

const express = require('express'),
    app = express(),
    port = process.env.PORT,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_URL);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


const employeeRoute = require('./src/api/routes/employee.route'),
    customerRoute = require('./src/api/routes/customer.route'),
    productRoute = require('./src/api/routes/product.route'),
    orderRoute = require('./src/api/routes/order.route');

employeeRoute(app);
customerRoute(app);
productRoute(app);
orderRoute(app);

app.listen(port);
console.log(`Administration started on: ${port}`)

