const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Customer = require('../models/customer.model');


exports.list = async (req, res) => {
    try {
        const customers = await Customer.aggregate([
            {
                $match: {
                    $and: req.conditions
                }
            },
            {
                $skip: req.query.skip
            },
            {
                $limit: req.query.limit
            }
        ]);

        return res.json({
            customers: customers.map(customer => new Customer(customer).transform())
        });
    } catch (error) {
        return (error);
    }
};

exports.detail = async (req, res) => {
    try {
        return res.json(req.locals.customer.transform());
    } catch (error) {
        return error;
    }
};

exports.update = async (req, res) => {
    try {
        req.body.updated_at = new Date();

        const customer = Object.assign(req.locals.customer, req.body);
        await customer.save();

        return res.json(customer.transform());
    } catch (error) {
        return (error);
    }
};

exports.delete = async (req, res) => {
    try {
        req.locals.customer.remove();
        return res.json({
            message: 'Đã xóa khách hàng thành công!',
            customer: req.locals.customer.transform()
        })
    } catch (error) {
        return error;
    }
};

exports.register = (req, res) => {
    const { name, username, password, birthday, address, phone, gender } = req.body;
    bcryptjs.hash(password, 10, (err, hash) => {
        if (err)
            res.status(400).send(err);
        else {
            const newCustomer = new Customer({
                _id: mongoose.Types.ObjectId(),
                name,
                username,
                password,
                birthday,
                address,
                phone,
                gender,
                hash_password: hash
            });
            newCustomer.save((err, customer) => {
                if (err)
                    res.status(400).send(err);
                else {
                    return res.json(customer.transform());
                }
            });
        }
    });
};

exports.login = async (req, res) => {
    await Customer.findOne(
        {
            username: req.body.username
        }, (err, customer) => {
            if (err) throw err;
            if (!customer) {
                res.json({ message: 'Tài khoản không tồn tại!' });
            }
            if (customer.password !== req.body.password) {
                res.json({ message: 'Mật khẩu không đúng!' });
            } else {
                res.json({ token: jwt.sign({ id: customer._id, username: customer.username, roles: customer.roles }, 'HoangHung', { expiresIn: '1d' }) });
            }
        }
    );
};

