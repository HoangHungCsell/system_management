const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Employee = require('../models/employee.model');


exports.list = async (req, res) => {
    try {
        const employees = await Employee.aggregate([
            {
                $match: {
                    $and: req.conditions
                }
            },
            {
                $skip: req.query.skip
            },
            {
                $limit: req.query.limit
            }
        ]);

        return res.json({
            employee: employees.map(employee => new Employee(employee).transform())
        });
    } catch (error) {
        return (error);
    }
};

exports.detail = async (req, res) => {
    try {
        return res.json(req.locals.employee.transform());
    } catch (error) {
        return error;
    }
};

exports.update = async (req, res) => {
    try {
        req.body.updated_at = new Date();

        const employee = Object.assign(req.locals.employee, req.body);
        await employee.save();

        return res.json(employee.transform());
    } catch (error) {
        return (error);
    }
};

exports.delete = async (req, res) => {
    try {
        req.locals.employee.remove();
        return res.json({
            message: 'Đã xóa nhân viên thành công!',
            employee: req.locals.employee.transform()
        })
    } catch (error) {
        return error;
    }
};

exports.register = (req, res) => {
    const { name, username, password, birthday, address, phone, gender, roles } = req.body;
    bcryptjs.hash(password, 10, (err, hash) => {
        if (err)
            res.status(400).send(err);
        else {
            const newEmployee = new Employee({
                _id: mongoose.Types.ObjectId(),
                name,
                username,
                password,
                birthday,
                address,
                phone,
                gender,
                roles,
                hash_password: hash
            });
            newEmployee.save((err, employee) => {
                if (err)
                    res.status(400).send(err);
                else {
                    return res.json(employee.transform());
                }
            });
        }
    });
};

exports.login = async (req, res) => {
    await Employee.findOne(
        {
            username: req.body.username
        }, (err, employee) => {
            if (err) throw err;
            if (!employee) {
                res.json({ message: 'Tài khoản không tồn tại!' });
            }
            if (employee.password !== req.body.password) {
                res.json({ message: 'Mật khẩu không đúng!' });
            } else {
                res.json({ token: jwt.sign({ id: employee._id, username: employee.username, roles: employee.roles }, 'HoangHung', { expiresIn: '1d' }) });
            }
        }
    );
};

