const Order = require('../models/order.model');
const Customer = require('../models/customer.model');
const { dropWhile, takeWhile } = require('lodash');

exports.create = async (req, res) => {
    try {
        const { quantity, price, discount } = req.body;
        req.body.created_by = {
            id: req.data.id,
            username: req.data.username
        }
        req.body.intoMoney = quantity * (price - discount);
        const order = new Order(req.body);

        Customer.findById(req.data.id, (err, customer) => {
            if (err)
                return err;
            customer.orders.push(order);
            customer.save();
        });

        await order.save();
        return res.json(order.transform());
    } catch (error) {
        return (error);
    }
};

exports.list = async (req, res) => {
    try {
        // const orders = await Order.aggregate([
        //     {
        //         $match: {
        //             $and: req.conditions
        //         }
        //     },
        //     {
        //         $sort: req.sorts
        //     },
        //     {
        //         $skip: req.query.skip
        //     },
        //     {
        //         $limit: req.query.limit
        //     }
        // ]);

        // return res.json({
        //     count: req.totalDocuments,
        //     orders: req.listOrd.orders.map(order => new Order(order).transform())
        // });
        return res.json({
            count: req.totalDocuments,
            orders: req.listOrd.order.map(order => new Order(order).transform())
        });
    } catch (error) {
        return (error);
    }
};

exports.detail = async (req, res) => {
    res.json(req.locals.order.transform());
};

exports.update = async (req, res) => {
    try {

        req.body.updated_at = new Date();
        Order.findById(req.params.orderId, async (err, order) => {
            if (err)
                return err;
            if (req.body.quantity)
                req.body.intoMoney = req.body.quantity * (order.price - order.discount);
            const cart = Object.assign(req.locals.order, req.body);
            Customer.findById(req.data.id, async (err, customer) => {
                console.log(customer.orders);

                if (err)
                    return err;
                orderCus = customer.orders.find(x => {
                    if (x._id === req.params.orderId)
                        return x;
                });

                const orderCustomer = Object.assign(orderCus, cart);

                await orderCustomer._doc.save();
            });
            await cart.save();
            return res.json(cart.transform());
        });

    } catch (error) {
        return (error);
    }
};

exports.delete = async (req, res) => {
    try {
        Customer.findById(req.data.id, (err, customer) => {
            if (err)
                return err;
            const a = JSON.parse(JSON.stringify(customer.orders));
            console.log(a);
            const orderCus = dropWhile(a, { _id: req.params.orderId });
            orderCus.save();
        });
        req.locals.order.remove();
        res.json({
            message: 'Đã xóa sản phẩm thành công!',
            order: req.locals.order.transform()
        })
    } catch (error) {
        return (error);
    }
};