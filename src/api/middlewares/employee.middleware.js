const Employee = require('../models/employee.model');

exports.condition = async (req, res, next) => {
    try {
        const params = req.query ? req.query : {};

        const conditions = [
            {
                $or: [
                    { name: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true } },
                    { username: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true } },

                ]
            },
            {
                address: params.address ? new RegExp(params.keyword, 'i') : { $exists: true }
            },
            {
                gender: params.genders
                    ? { $in: params.genders }
                    : { $exists: true }
            },
            {
                roles: params.roles
                    ? { $in: params.roles }
                    : { $exists: true }
            }
        ];

        req.conditions = conditions;
        return next();
    } catch (error) {
        return (error);
    }
};

exports.load = async (req, res, next) => {
    try {
        const employee = await Employee.getEmployeeById(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.employee = employee;
        return next();
    } catch (error) {
        return error;
    }
};