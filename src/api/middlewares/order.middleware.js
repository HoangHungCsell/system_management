const Order = require('../models/order.model');

exports.condition = async (req, res, next) => {
    try {
        const params = req.query ? req.query : {};
        let start; let end;
        if (params.start_time && params.end_time) {
            start = new Date(params.start_time); end = new Date(params.end_time);
            start.setHours(0, 0, 0, 0); end.setHours(23, 59, 59, 999);
        }


        const conditions = [
            {
                categories: params.categories
                    ? { $in: params.categories }
                    : { $exists: true }
            },
            {
                unit: params.units
                    ? { $in: params.units }
                    : { $exists: true }
            },
            {
                currency: params.currency ? params.currency : { $exists: true }
            },
            {
                name: params.keyword ? new RegExp(params.keyword, 'i') : { $exists: true }
            },

            {
                price: params.min_price
                    ? { $gte: params.min_price }
                    : { $exists: true }
            },
            {
                price: params.max_price
                    ? { $lte: params.max_price }
                    : { $exists: true }
            },
            {
                updated_at: params.date_by === 'update' && start && end
                    ? { $gte: start, $lte: end }
                    : { $exists: true }
            },
            {
                created_at: params.date_by === 'create' && start && end
                    ? { $gte: start, $lte: end }
                    : { $exists: true }
            }

        ];
        req.conditions = conditions;

        return next();
    } catch (error) {
        return (error);
    }
};

exports.load = async (req, res, next) => {
    try {
        const order = await Order.getOrderById(req.params.orderId);

        req.locals = req.locals ? req.locals : {};
        req.locals.order = order;

        return next();
    } catch (error) {
        return (error);
    }
};

exports.listOrd = async (req, res, next) => {
    try {
        const order = await Order.find({ 'created_by.id': req.data.id });

        req.listOrd = req.listOrd ? req.listOrd : {};
        req.listOrd.order = order;

        return next();
    } catch (error) {
        return error;
    }
};

exports.count = async (req, res, next) => {
    try {
        req.totalDocuments = await Order.find({ $and: req.conditions }).countDocuments();
        return next();
    } catch (error) {
        return (error);
    }
};

exports.sort = (req, res, next) => {
    const params = req.query ? req.query : {};
    const sort = params.order_by === 'desc' ? -1 : 1;
    switch (params.sort_by) {
        case 'name':
            req.sorts = { name: sort };
            break;
        case 'price':
            req.sorts = { price: sort };
            break;
        case 'create':
            req.sorts = { created_at: sort };
            break;
        case 'update':
            req.sorts = { updated_at: sort };
            break;
        default:
            req.sorts = { name: sort };
            break;
    }
    next();
};

