
const Employee = require('../models/employee.model');
const Customer = require('../models/customer.model');
const Order = require('../models/order.model');
function authorizeEmp(roles) {
    return [
        (req, res, next) => {
            Employee.findById(req.data.id, function (error, employee) {
                if (error)
                    return (error);
                if (!employee) return res.json({ message: 'Tài khoản không tồn tại!' });
                const check = employee.roles.find(x => x === roles);

                if (!check) return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
                next();
            });

        }
    ];
}

function authorizeCus(roles) {
    return [
        (req, res, next) => {
            // if (req.data.id === Employee.findById(req.data.id)) {
            //     Employee.findById(req.data.id, (error, employee) => {
            //         if (error)
            //             return (error);
            //         if (!employee) return res.json({ message: 'Tài khoản không tồn tại!' });
            //         const check = employee.roles.find(x => x === roles);

            //         if (!check) return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
            //         next();
            //     });
            // }
            // if (req.data.id === Customer.findById(req.data.id)) {
            //     Customer.findById(req.data.id, (error, customer) => {
            //         if (error)
            //             return (error);
            //         if (!customer) return res.json({ message: 'Tài khoản không tồn tại!' });
            //         const check = customer.roles.find(x => x === roles);

            //         if (!check || req.data.id !== req.params.id)
            //             return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
            //         next();
            //     });
            // }



            Customer.findById(req.data.id, (error, customer) => {
                if (error)
                    return (error);
                if (!customer) return res.json({ message: 'Tài khoản không tồn tại!' });
                const check = customer.roles.find(x => x === roles);

                if (req.data.id !== req.params.id || !check)
                    return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
                next();
            });

        }
    ];
}

function authorizeOrd(roles) {
    return [
        (req, res, next) => {
            Order.findById(req.params.orderId, (err, order) => {

                if (err)
                    return err;
                if (req.data.id !== order.created_by.id)
                    return res.status(401).json({ message: 'Bạn không có quyền truy cập hoặc thực hiện thao tác này!' });
                next();
            });


        }
    ];
}

module.exports = {
    authorizeEmp,
    authorizeCus,
    authorizeOrd
};