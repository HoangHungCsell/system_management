const mongoose = require('mongoose');
const Order = require('../models/order.model');

const customerSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        username: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            minlength: 6,
            required: true
        },
        hash_password: String,
        birthday: {
            type: Date,
            required: true
        },
        address: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            unique: true,
            minlength: 10,
            required: true
        },
        gender: {
            type: String,
            required: true
        },
        roles: {
            type: [String],
            default: ['read', 'update', 'delete']
        },
        orders: {
            type: [Object],
            default: []
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        timestamps: false,
        versionKey: false
    }
);

customerSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            'id',
            'name',
            'username',
            'birthday',
            'address',
            'phone',
            'gender',
            'roles',
            'orders',
            'created_at',
            'updated_at'
        ];
        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

customerSchema.static({
    async getCustomerById(id) {
        try {
            const customer = await this.findById(id);
            if (!customer)
                return 'Không tìm thấy dữ liệu';
            return customer;
        } catch (error) {
            return error;
        }
    }
});

module.exports = mongoose.model('Customer', customerSchema, 'customers');