const mongoose = require('mongoose');

const employeeSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        username: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            minlength: 6,
            required: true
        },
        hash_password: String,
        birthday: {
            type: Date,
            required: true
        },
        address: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            unique: true,
            minlength: 10,
            required: true
        },
        gender: {
            type: String,
            required: true
        },
        roles: {
            type: [String],
            default: 'read'
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        timestamps: false,
        versionKey: false
    }
);

employeeSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            'id',
            'name',
            'username',
            'birthday',
            'address',
            'phone',
            'gender',
            'roles',
            'created_at',
            'updated_at'
        ];
        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

employeeSchema.static({
    async getEmployeeById(id) {
        try {
            const employee = await this.findById(id);
            if (!employee)
                return 'Không tìm thấy dữ liệu';
            return employee;
        } catch (error) {
            return error;
        }
    }
});

module.exports = mongoose.model('Employee', employeeSchema, 'employees');