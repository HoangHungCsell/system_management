const mongoose = require('mongoose');
const { values } = require('lodash');
const { mongo } = require('mongoose');

const Currencies = {
    VND: 'VND',
    USD: 'USD'
};

const Units = {
    BO: 'Bộ',
    CAI: 'Cái',
    DOI: 'Đôi',
    CHIEC: 'Chiếc'
};



const orderSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: false
        },
        image: {
            type: [String],
            required: true
        },
        name: {
            type: String,
            required: true
        },
        unit: {
            type: String,
            enum: values(Units),
            default: Units.CAI
        },
        quantity: {
            type: Number,
            default: 1
        },
        intoMoney: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            maxlength: 500,
            default: null
        },
        price: {
            type: Number,
            required: true
        },
        discount: {
            type: Number,
            default: 0
        },
        currency: {
            type: String,
            uppercase: true,
            enum: values(Currencies),
            default: Currencies.VND
        },
        categories: {
            type: [String],
            required: true
        },
        created_by: {
            id: {
                type: String,
                required: true
            },
            username: {
                type: String,
                required: true
            }
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        timestamps: false,
        versionKey: false
    }
);

orderSchema.pre('save', async function save(next) {
    try {
        this.wasNew = this.isNew;
        if (this.wasNew) {
            this._id = new mongo.ObjectId().toHexString();
        }
        return next();
    } catch (error) {
        return (error);
    }
});

orderSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            '_id',
            'image',
            'name',
            'unit',
            'quantity',
            'intoMoney',
            'description',
            'price',
            'discount',
            'currency',
            'categories',
            'created_by',
            'created_at',
            'updated_at'
        ];

        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

orderSchema.static({
    Currencies,
    Units,
    async getOrderById(id) {
        try {
            const order = this.findById(id);

            if (!order)
                return 'Không tìm thấy dữ liệu';
            return order;

        } catch (error) {
            return (error);
        }
    }
});

module.exports = mongoose.model('Order', orderSchema, 'orders');