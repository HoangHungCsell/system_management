const mongoose = require('mongoose');
const { values } = require('lodash');
const { mongo } = require('mongoose');

const Currencies = {
    VND: 'VND',
    USD: 'USD'
};

const Units = {
    BO: 'Bộ',
    CAI: 'Cái',
    DOI: 'Đôi',
    CHIEC: 'Chiếc'
};



const productSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            required: false
        },
        image: {
            type: [String],
            required: true
        },
        name: {
            type: String,
            required: true
        },
        unit: {
            type: String,
            enum: values(Units),
            default: Units.CAI
        },
        description: {
            type: String,
            maxlength: 500,
            default: null
        },
        price: {
            type: Number,
            required: true
        },
        discount: {
            type: Number,
            default: null
        },
        currency: {
            type: String,
            uppercase: true,
            enum: values(Currencies),
            default: Currencies.VND
        },
        categories: {
            type: [String],
            required: true
        },
        created_by: {
            id: {
                type: String,
                required: true
            },
            username: {
                type: String,
                required: true
            }
        },
        created_at: {
            type: Date,
            default: () => new Date()
        },
        updated_at: {
            type: Date,
            default: () => new Date()
        }
    },
    {
        timestamps: false,
        versionKey: false
    }
);

productSchema.pre('save', async function save(next) {
    try {
        this.wasNew = this.isNew;
        if (this.wasNew) {
            this._id = new mongo.ObjectId().toHexString();
        }
        return next();
    } catch (error) {
        return (error);
    }
});

productSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            '_id',
            'image',
            'name',
            'unit',
            'description',
            'price',
            'discount',
            'currency',
            'categories',
            'created_by',
            'created_at',
            'updated_at'
        ];

        fields.forEach(field => {
            transformed[field] = this[field];
        });
        return transformed;
    }
});

productSchema.static({
    Currencies,
    Units,
    async getProductById(id) {
        try {
            const product = this.findById(id);

            if (!product)
                return 'Không tìm thấy dữ liệu';
            return product;

        } catch (error) {
            return (error);
        }
    }
});

module.exports = mongoose.model('Product', productSchema, 'products');