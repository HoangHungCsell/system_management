
module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/customer.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/customer.middleware');
    const { authorizeEmp, authorizeCus } = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/customer.validation');


    app.route('/customers')
        .get(
            auth,
            authorizeEmp('list'),
            validate(listValidation),
            middleware.condition,
            controller.list
        );

    app.route('/customers/:id')
        .get(
            auth,
            authorizeCus('read'),
            middleware.load,
            controller.detail
        )
        .put(
            auth,
            authorizeCus('update'),
            validate(updateValidation),
            middleware.load,
            controller.update
        )
        .delete(
            auth,
            authorizeCus('delete'),
            middleware.load,
            controller.delete
        );

    app.route('/registerCus')
        .post(
            validate(createValidation),
            controller.register
        );

    app.route('/loginCus')
        .post(controller.login);
};