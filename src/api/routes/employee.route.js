
module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/employee.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/employee.middleware');
    const { authorizeEmp } = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/employee.validation');


    app.route('/employees')
        .get(
            auth,
            authorizeEmp('list'),
            validate(listValidation),
            middleware.condition,
            controller.list
        );

    app.route('/employees/:id')
        .get(
            auth,
            authorizeEmp('read'),
            middleware.load,
            controller.detail
        )
        .put(
            auth,
            authorizeEmp('update'),
            validate(updateValidation),
            middleware.load,
            controller.update
        )
        .delete(
            auth,
            authorizeEmp('delete'),
            middleware.load,
            controller.delete
        );

    app.route('/register')
        .post(
            validate(createValidation),
            controller.register
        );

    app.route('/login')
        .post(controller.login);
};