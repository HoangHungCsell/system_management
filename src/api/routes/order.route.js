module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/order.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/order.middleware');
    const { authorizeOrd } = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/order.validation');

    app.route('/orders')
        .get(
            auth,
            validate(listValidation),
            middleware.listOrd,
            // middleware.condition,
            // middleware.count,
            // middleware.sort,
            controller.list
        )
        .post(
            auth,
            validate(createValidation),
            controller.create
        );
    app.route('/orders/:orderId')
        .get(
            auth,
            authorizeOrd('read'),
            middleware.load,
            controller.detail
        )
        .put(
            auth,
            authorizeOrd('update'),
            validate(updateValidation),
            middleware.load,
            controller.update)
        .delete(
            auth,
            authorizeOrd('delete'),
            middleware.load,
            controller.delete
        );
};