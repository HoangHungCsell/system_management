module.exports = app => {
    const validate = require('express-validation');
    const controller = require('../controllers/product.controller');
    const auth = require('../middlewares/login.middleware');
    const middleware = require('../middlewares/product.middleware');
    const { authorizeEmp } = require('../middlewares/permission.middleware');
    const {
        listValidation,
        createValidation,
        updateValidation
    } = require('../validations/product.validation');

    app.route('/products')
        .get(
            auth,
            authorizeEmp('list'),
            validate(listValidation),
            middleware.condition,
            middleware.count,
            middleware.sort,
            controller.list
        )
        .post(
            auth,
            authorizeEmp('create'),
            validate(createValidation),
            controller.create
        );
    app.route('/products/:productId')
        .get(
            auth,
            authorizeEmp('read'),
            middleware.load,
            controller.detail
        )
        .put(
            auth,
            authorizeEmp('update'),
            validate(updateValidation),
            middleware.load,
            controller.update)
        .delete(
            auth,
            authorizeEmp('delete'),
            middleware.load,
            controller.delete
        );
};