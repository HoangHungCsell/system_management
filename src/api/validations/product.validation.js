const Joi = require('joi');
const { values } = require('lodash');
const Product = require('../models/product.model');

module.exports = {
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .default(10),

            categories: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            unit: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            currency: Joi.string()
                .allow(null, ''),
            keyword: Joi.string()
                .allow(null, ''),
            min_price: Joi.number()
                .allow(null, ''),
            max_price: Joi.number()
                .allow(null, ''),

            date_by: Joi.string()
                .only(['create', 'update'])
                .default('create')
                .allow(null, ''),
            start_time: Joi.date()
                .allow(null, ''),
            end_time: Joi.date()
                .allow(null, ''),

            sort_by: Joi.string()
                .only(['name', 'price', 'create', 'update'])
                .allow(null, '')
                .default('name'),
            order_by: Joi.string()
                .only(['asc', 'desc'])
                .allow(null, '')
                .default('asc')
        }
    },
    createValidation: {
        body: {
            image: Joi.array()
                .items(Joi.string())
                .required(),
            name: Joi.string()
                .required(),
            unit: Joi.string()
                .only(values(Product.Units))
                .default(Product.Units.CAI),
            description: Joi.string()
                .max(500)
                .default(null),
            price: Joi.number()
                .required(),
            discount: Joi.number()
                .default(null),
            currency: Joi.string()
                .uppercase()
                .only(values(Product.Currencies))
                .default(Product.Currencies.VND),
            categories: Joi.array()
                .items(Joi.string())
                .required()
        }
    },
    updateValidation: {
        body: {
            image: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            name: Joi.string()
                .allow(null, ''),
            unit: Joi.string()
                .only(values(Product.Units))
                .allow(null, ''),
            description: Joi.string()
                .max(500)
                .allow(null, ''),
            price: Joi.number()
                .allow(null, ''),
            discount: Joi.number()
                .allow(null, ''),
            currency: Joi.string()
                .uppercase()
                .only(values(Product.Currencies))
                .allow(null, ''),
            categories: Joi.array()
                .items(Joi.string())
                .allow(null, '')
        }
    }
};